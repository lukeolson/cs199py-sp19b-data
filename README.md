# cs199py-sp19b-data

| data | source |
| ---- | ------ |
| 2008.csv | http://stat-computing.org/dataexpo/2009/the-data.html |
| 2019.csv | https://www.transtats.bts.gov/DL_SelectFields.asp |
